#!/usr/bin/python
import serial, time
import argparse
import sys, threading
import textwrap
from reprint import output

class EbdUsb:
  is_running = True
  data = []
  def __init__(self, serial_port, file = -1, verbose = False):
    self.ser = serial.Serial( serial_port, 9600, 
                              serial.EIGHTBITS, 
                              serial.PARITY_EVEN, 
                              serial.STOPBITS_ONE, 
                              timeout = 1)
    self.thread = threading.Thread(target=self.read_serial, args=())
    self.thread.start()
    self.last_message = ""
    self.start_time = -1
    self.last_time = -1
    self.capacity = 0
    self.energy = 0
    self.file = file
    self.store_to_file = False
    if file != -1: self.store_to_file = True
    self.verbose = verbose
    self.avalible = False

  def avalible(self):
    return self.avalible

  def read(self):
    out = self.data[0]
    self.data.pop(0)
    if len(self.data) == 0: self.avalible = False
    # print out, self.avalible
    return out

  def connect(self):
    self.send("FA0500000000000005F8") 

  def start(self, voltage, amperage, time):
    data = self.int_to_hex(int(round(amperage*1000*0.968992248)))+self.int_to_hex(int(round(voltage*100.0*1.059)))+self.int_to_hex(time)
    self.send_unknown("FA01"+data+"--F8", "fa0a")

  def end(self):
    self.send("FA0200000000000002F8")
    self.file.close()

  def disconnect(self):
    self.send("FA0600000000000006F8")

  def store(self, data):
    if (data[:4].upper() == "FA0A"):
      if self.start_time == -1:
        self.start_time = int(round(time.time() * 1000))
        self.last_time = self.start_time * 1
        tmp_str = "Time     |Capacity   |Energy     |Voltage   |Current   |Cut-off voltage |Set current |Cut-off time"
        if self.store_to_file:
          self.file.write(tmp_str.replace("|",",")+"\n")
        if self.verbose:
          print tmp_str

      data = [int(i, 16) for i in textwrap.wrap(data, 4)]

      voltage = data[2]*0.001*0.922826592
      amperage = data[1]*0.0001
      delta_time = int(round(time.time() * 1000)) - self.last_time
      self.energy += voltage * amperage * delta_time / 3600
      self.capacity += amperage * delta_time / 3600
      self.last_time = int(round(time.time() * 1000))
      
      self.data.append({"time":       (self.last_time - self.start_time)/1000,
                   "capacity":        self.capacity,
                   "energy":          self.energy,
                   "voltage":         voltage,
                   "current":         amperage,
                   "cut-off_voltage": data[6]*0.01/1.059,
                   "cut-off_time":    data[7],
                   "set_current":     data[5]*0.001/0.968992248})
      self.avalible = True

      tmp_str = " {5:6d}s | {6:6d}mAh | {7:6d}mWh | {0:6.3f}V  |  {1:2.3f}A  |      {2:2.2f}V     |   {3:2.3f}A   | {4:4d}min  ".format(
                        voltage, 
                        amperage,
                        data[6]*0.01/1.059,
                        data[5]*0.001/0.968992248,
                        data[7],
                        (self.last_time - self.start_time)/1000,
                        int(self.capacity),
                        int(self.energy))
      if self.store_to_file:
        self.file.write(tmp_str.replace("|", ",")+"\n")
      if self.verbose:
        print "\r"+tmp_str,
        sys.stdout.flush()

    else:
      if data[:4].upper() == "FA00": 
        self.is_running = False
        self.end()
        self.disconnect()
        sys.exit()
      # print "\n", data

  def read_serial(self):
    tmp_data = ""
    while self.is_running == True:
      tmp = self.ser.read()
      if len(tmp) > 0:
        tmp_data += self.bin_to_hex(tmp)#+"|"
        
        if ord(tmp) == 248: # End of message
          self.last_message = tmp_data
          self.store(tmp_data)
          tmp_data = ""

  def send(self,send_data, response= "", timeout = 1):
    self.ser.write(self.hex_to_byte(send_data))
    if len(response) > 0:
      while "_"+response not in "_"+self.last_message:
        time.sleep(0.001)

  def send_unknown(self, send_data, response, timeout=0.001):
    for i in range(256):
      tmp = send_data[:-4]+hex(i)[2:].zfill(2)+send_data[-2:]
    
      self.ser.write(self.hex_to_byte(tmp))
      time.sleep(timeout)
      if "_"+response.upper() in "_"+self.last_message.upper():
        return

  def hex_to_byte(self,hex_data):
    tmp_data = []
    for i in range(0, len(hex_data),2):
      tmp_data.append(int(hex_data[i:i+2],16))
    return bytearray(tmp_data)

  def bin_to_hex(self, bin_data):
    return hex(ord(bin_data))[2:].zfill(2)

  def int_to_hex(self,int_data):
    return (hex(int_data)[2:].zfill(4))

def main():
  parser = argparse.ArgumentParser(description = "Simple EBD_USB library.")

  parser.add_argument('-a', '--amperage', action='store',
                    type=float,
                    default= 0,
                    dest='amperage',
                    help='How many amperage to draw')

  parser.add_argument('-o', '--output', action='store',
                    type=argparse.FileType('w'),
                    default= -1,
                    dest='file',
                    help='Where to store raw data')

  parser.add_argument('-p', '--port', action='store',
                    dest='serial_port',
                    help='Select port',
                    required = True)

  parser.add_argument('-t', '--time', action='store',
                    default = 0,
                    type=int,
                    dest='time',
                    help='Max. time to cut-off [minutes]')

  parser.add_argument('-v', '--voltage', action='store',
                    default = 0,
                    type=float,
                    dest='voltage',
                    help='Cut-off voltage')

  results = parser.parse_args()

  try:
    ebd = EbdUsb(results.serial_port, results.file, verbose=True)


    ebd.connect()
    ebd.start(results.voltage, results.amperage, results.time)
    while (ebd.is_running): 
      time.sleep(0.1)
      # while (ebd.avalible):
      #   print ebd.read()

  except:
    ebd.is_running = False
    ebd.end()
    ebd.disconnect()

if __name__ == "__main__":
    main()