# Cross-platform EBD-USB+ interface
This is simple python interface for EBD-USB+ electronic load. It can be used as an external library, or as a standalone program.
### Usage
For the instruction type: `python ebd_usb.py -h`
### Example
For example, if you would like to test AAA battery with 400mA current, cut-off voltage of 0.8V and store the measurements to the "AA_results.csv" try this command:

```python ebd_usb.py -p "[PORT]" -a 0.4 -v 0.8 -o AA_results.csv ```

Don't forget to change -p flag [PORT] with the correct serial port path (on Linux something like "/dev/ttyUSB0", on Windows devices something like "COM1").
### TODO
- Fix CRC calculation (currently brute force approach is used)
- Add measurements update without stopping the electronic load.

### More information
[Link](http://tomaz.kompara.si/projects/Software/EBD-USB/index.html) to the project website.
