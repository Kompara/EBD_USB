char dataString[3]; // Buffer for HEX values

void setup() {
  // Begin all serials
  Serial.begin(19200);
  Serial1.begin(9600, SERIAL_8E1);
  Serial2.begin(9600, SERIAL_8E1);
}

void loop() {
  // Check if something was sent to the device
  if (Serial1.available()){
    dataString[0] = "0";
    Serial.print("S: ");
    while (strcmp(dataString, "F8")) {
      if (Serial1.available()){
        sprintf(dataString,"%02X",Serial1.read());
        Serial.print(dataString);
      } 
    }
    Serial.println("");
  }

  // Check if device send something back
  if (Serial2.available()){
    dataString[0] = "0";
    Serial.print("R: ");
    while (strcmp(dataString, "F8")) {
      if (Serial2.available()){
        sprintf(dataString,"%02X",Serial2.read());
        Serial.print(dataString);
      } 
    }
    Serial.println("");
  }
}
